#! /usr/bin/python
# -*- coding: utf-8 -*-
from itertools import permutations
import re
import os
from operator import add, sub, mul, truediv
import sys

__LEN__ = 4
__OPERATORS__ = ['+', '-', '*', '/']  # [add, sub, mul, truediv]
__OPERATORS_LEN__ = len(__OPERATORS__)
__BRACKETS_LEN__ = __LEN__
__TOTAL_LEN__ = 15
__TEN__ = 10
__e__ = 1.0e-5

# 0 1 2 3 4 5 6 7 8 9 10 11
# ( n ) + n ) + n ) + n  )
# 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14
# ( n ) + ( n ) + ( n )  +  (  n  )
# NUM_IDXS = [1, 4, 7, 10]
# OPS_IDXS = [3, 6, 9]
# RB_IDXS = [0, 2, 5, 8, 11]

# 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14
# ( n ) + ( n ) + ( n )  +  (  n  )
NUM_IDXS = [1, 5, 9, 13]
OPS_IDXS = [3, 7, 11]
RB_START_IDXS = [0, 4, 8, 12]
RB_END_IDXS = [2, 6, 10, 14]


def sort_by_random_numbers(numbers):
    assert(len(numbers) == __LEN__)
    return permutations(numbers, __LEN__)

def sort_by_random_operators():
    for op1 in __OPERATORS__:
        for op2 in __OPERATORS__:
            for op3 in __OPERATORS__:
                yield (op1, op2, op3)

def inject_round_brackets():
    for i in range(__BRACKETS_LEN__):
        for j in range(i+1, __BRACKETS_LEN__):
            yield (i, j)

def translate(expressions, _sep=''):
    sep = _sep
    return sep.join(map(lambda x: x if x is not None else '', expressions))

def calc(expressions):
    exp = translate(expressions)
    ans = 0
    ans = eval(exp)
    return ans

def equalsTen(num):
    return abs(num - __TEN__) < __e__

def is_div0(expressions, op, opidx):
    # if not (op == '/' and opidx + 1 < __TOTAL_LEN__):
    #     return False
    # if expressions[OPS_IDXS[opidx] + 1] == '0':
    #     return True
    # return False
    if op != '/':
        return False
    if opidx + 1 >= __TOTAL_LEN__:
        return False
    if expressions[opidx + 1] != '0':
        return False
    r_range = None
    for i in range(len(expressions)):
        if expressions[i] != '(':
            continue
        for j in range(i+1, len(expressions)):
            if expressions[j] != ')':
                continue
            r_range = (i, j)
            break
    if opidx <= r_range[0] or opidx >= r_range[1]:
        return False
    if eval(translate(expressions[i+1:j])) != 0:
        return False
    return True

    
def solve(numbers):
    for nums in sort_by_random_numbers(numbers):
        for ops in sort_by_random_operators():
            expressions1 = [None] * __TOTAL_LEN__
            
            for i, num in enumerate(nums):
                expressions1[NUM_IDXS[i]] = str(num)

            can_calc = True
            for i, op in enumerate(ops):
                # if is_div0(expressions1, op, OPS_IDXS[i]):
                #     can_calc = False
                #     break
                expressions1[OPS_IDXS[i]] = op
            if not can_calc:
                continue

            try:
                if equalsTen(calc(expressions1)):
                    return (True, translate(expressions1, ' '))
            except ZeroDivisionError:
                pass

            for rb in inject_round_brackets():
                expressions2 = expressions1[:]

                expressions2[RB_START_IDXS[rb[0]]] = '('
                expressions2[RB_END_IDXS[rb[1]]] = ')'
                try:
                    if equalsTen(calc(expressions2)):
                        return (True, translate(expressions2, ' '))
                except ZeroDivisionError:
                    pass
            
    return (False, '')

if __name__ == '__main__':
    data = sys.argv[1:]
    ans = solve(list(map(lambda x: int(x), data[:])))
    print('YNeos'[not ans[0]::2], ''.join([' -> ' if ans[0] else '',  ans[1]]))

# EOF
